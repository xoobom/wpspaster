# node

node18

# 下载依赖

```
npm config set registry https://registry.npmmirror.com/
npm install
```

# 运行

```
npm run dev
```

# 打包

```
npm run build:win
npm run build:mac
```

# electron f12

mac为command + option + i。win为ctrl+shift+i
