import { createApp } from 'vue';
import App from './App.vue';
import '@renderer/styles/index.less';

createApp(App).mount('#app');
