# node

node版本18

# 安装依赖

```bush
npm config set registry https://registry.npmmirror.com/
npm install
```

# 运行

```
npm run dev
```

# 打包

```
npm run build
```

# ckeditor
https://ckeditor.com/ckeditor-5/online-builder/里选，然后下载压缩包，解压，npm i然后npm run build，然后复制到/public/resource/ckeditor。最后npm i ./public/resource/ckeditor添加本地包
必选插件：Simple upload adapter、Font background color、Font color、
Font family、Font size、
